# 2023 - Nix

To get the (eval-time-baked!) answer for a day and part:

```bash
❯ answer 01 1
56042
```

To run the tests:
```bash
❯ nixt

Found 14 cases in 6 suites over 1 files.

┏ /nix/store/l89bvdw4qamcac4gasi1jxwqhlzcvp5-source/day01/test.nix
┃   strToChars
┃     ✓ hello -> (h,e,l,l,o)
...
```
