{
  description = "AoC 2023";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixt = {
    url = "github:kylesferrazza/nixt/fix-settings-being-ignored";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    flake-utils,
    nixt,
  }: let
    system-outputs = flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
      blockFor = testPath: dayPath:
        nixt.lib.block testPath (import testPath {
          inherit (nixt.lib) describe';
          day = import dayPath {inherit (pkgs) lib;};
        });
    in rec {
      devShell = pkgs.mkShell {
        name = "advent-2023";
        buildInputs = [
          nixt.packages.${system}.default
          pkgs.alejandra
          (pkgs.writeShellScriptBin "answer" ''
            set -euo pipefail
            DAY="$1"
            PART="$2"
            CURRENT_SYSTEM="$(nix eval --raw --impure --expr 'builtins.currentSystem')"
            nix eval --raw ".#days.$CURRENT_SYSTEM.day$DAY.part$PART"
            echo
          '')
        ];
      };

      days = {
        day01 = import ./day01 {inherit (pkgs) lib;};
      };

      formatter = pkgs.alejandra;
      nixtConfig = nixt.lib.grow {
        blocks = [
          (blockFor ./day01/test.nix ./day01)
        ];
        settings = {
          "verbose" = true;
          "list" = false;
          "trace" = false;
          "watch" = false;
        };
      };
    });
  in
    system-outputs
    // {
      __nixt = system-outputs.nixtConfig.x86_64-linux;
    };
}
