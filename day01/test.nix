{
  day,
  describe',
  ...
}: let
  inherit
    (day)
    calibrationSum
    calibrationValue
    prefixNumber
    calibrationSum'
    calibrationValue'
    strToChars
    firstNumber
    lastNumber
    isNumber
    part1
    part2
    firstNumber'
    lastNumber'
    numMap
    ;
in [
  (describe' "isNumber" {
    "a -> no" = ! (isNumber "a");
    "1 -> yes" = isNumber "1";
  })
  (describe' "firstNumber" {
    "1b2c3 -> 1" = (firstNumber "1b2c3") == "1";
    "a1b2c3 -> 1" = (firstNumber "a1b2c3") == "1";
    "abc -> null" = (firstNumber "abc") == null;
  })
  (describe' "lastNumber" {
    "a1b2c3 -> 3" = (lastNumber "a1b2c3") == "3";
    "a1b2c3d -> 3" = (lastNumber "a1b2c3d") == "3";
    "abc -> null" = (lastNumber "abc") == null;
  })
  (describe' "calibrationValue" {
    "1abc2 -> 12" = (calibrationValue "1abc2") == 12;
    "pqr3stu8vwx -> 38" = (calibrationValue "pqr3stu8vwx") == 38;
    "a1b2c3d4e5f -> 15" = (calibrationValue "a1b2c3d4e5f") == 15;
    "treb7uchet -> 77" = (calibrationValue "treb7uchet") == 77;
  })
  (describe' "calibrationSum" {
    "all of the above -> 142" =
      (calibrationSum ''
        1abc2
        pqr3stu8vwx
        a1b2c3d4e5f
        treb7uchet
      '')
      == 142;
  })
  (describe' "part1" {
    "answer -> 56042" = part1 == "56042";
  })
  (describe' "prefixNumber" {
    "two -> 2" =
      (prefixNumber numMap "two")
      == {
        present = true;
        digit = "2";
      };
    "2 -> 2" =
      (prefixNumber numMap "2")
      == {
        present = true;
        digit = "2";
      };
    "a -> NaN" = (prefixNumber numMap "a") == {present = false;};
    "two123 -> 2" =
      (prefixNumber numMap "two123")
      == {
        present = true;
        digit = "2";
      };
    "4one -> 4" =
      (prefixNumber numMap "4one")
      == {
        present = true;
        digit = "4";
      };
  })
  (describe' "firstNumber'" {
    "two1nine -> 2" = (firstNumber' "two1nine") == "2";
    "xtwone3four -> 2" = (firstNumber' "xtwone3four") == "2";
    "xtwone3fourx -> 2" = (firstNumber' "xtwone3fourx") == "2";
    "4nineeightseven2 -> 4" = (firstNumber' "4nineeightseven2") == "4";
    "a -> null" = (firstNumber' "a") == null;
  })
  (describe' "lastNumber'" {
    "two1nine -> 9" = (lastNumber' "two1nine") == "9";
    "xtwone3four -> 4" = (lastNumber' "xtwone3four") == "4";
    "xtwone3fourx -> 4" = (lastNumber' "xtwone3fourx") == "4";
    "4nineeightseven2 -> 2" = (lastNumber' "4nineeightseven2") == "2";
    "a -> null" = (lastNumber' "a") == null;
  })
  (describe' "calibrationValue'" {
    "two1nine -> 29" = (calibrationValue' "two1nine") == 29;
    "eightwothree -> 83" = (calibrationValue' "eightwothree") == 83;
    "abcone2threexyz -> 13" = (calibrationValue' "abcone2threexyz") == 13;
    "xtwone3four -> 24" = (calibrationValue' "xtwone3four") == 24;
    "4nineeightseven2 -> 42" = (calibrationValue' "4nineeightseven2") == 42;
    "zoneight234 -> 14" = (calibrationValue' "zoneight234") == 14;
    "7pqrstsixteen -> 76" = (calibrationValue' "7pqrstsixteen") == 76;
  })
  (describe' "calibrationSum'" {
    "all of the above -> 281" =
      (calibrationSum' ''
        two1nine
        eightwothree
        abcone2threexyz
        xtwone3four
        4nineeightseven2
        zoneight234
        7pqrstsixteen
      '')
      == 281;
  })
  (describe' "part2" {
    "answer -> 55358" = part2 == "55358";
  })
]
