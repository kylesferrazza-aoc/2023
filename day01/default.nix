{lib, ...}: rec {
  isNumber = c: (builtins.tryEval (lib.strings.toInt c)).success;
  firstNumberL = l: lib.lists.findFirst isNumber null l;
  firstNumber = s: firstNumberL (lib.strings.stringToCharacters s);
  lastNumberL = l: firstNumberL (lib.lists.reverseList l);
  lastNumber = s: lastNumberL (lib.strings.stringToCharacters s);
  calibrationValueGeneric = findFirstFunc: findLastFunc: s: let
    f = findFirstFunc s;
    l = findLastFunc s;
    strAnswer = "${f}${l}";
    intAnswer = lib.strings.toInt strAnswer;
  in
    intAnswer;
  calibrationSumGeneric = calibrationValueFunc: s: let
    lines = lib.strings.splitString "\n" s;
    nonEmptyLines = lib.lists.remove "" lines;
    sums = map calibrationValueFunc nonEmptyLines;
    add = a: b: a + b;
    total = lib.lists.foldr add 0 sums;
  in
    total;
  calibrationValue = calibrationValueGeneric firstNumber lastNumber;
  calibrationSum = calibrationSumGeneric calibrationValue;

  inputContents = builtins.readFile ./input.txt;
  calibrationSumForInput = sumF: builtins.toString (sumF inputContents);

  part1 = calibrationSumForInput calibrationSum;

  numMap = {
    "one" = "1";
    "two" = "2";
    "three" = "3";
    "four" = "4";
    "five" = "5";
    "six" = "6";
    "seven" = "7";
    "eight" = "8";
    "nine" = "9";
  };

  revString = s: let
    chars = lib.strings.stringToCharacters s;
    revChars = lib.lists.reverseList chars;
    ans = lib.strings.concatStrings revChars;
  in
    ans;

  numMapRev =
    lib.attrsets.concatMapAttrs (name: value: {
      ${revString name} = value;
    })
    numMap;

  prefixNumber = numMapG: s: let
    txtNums = builtins.attrNames numMapG;
    digNums = builtins.attrValues numMapG;
    isDigNum = x: lib.lists.any (y: x == y) digNums;
    isTxtNum = x: lib.lists.any (y: x == y) txtNums;
    chars = lib.strings.stringToCharacters s;
    startingTxtNum = lib.lists.findFirst (txtNum: (lib.strings.hasPrefix txtNum s)) false txtNums;
    sStartsTxtNum = startingTxtNum != false;
    firstChar = builtins.elemAt chars 0;
    sStartsDigNum = isDigNum firstChar;
    ans =
      if sStartsDigNum
      then {
        present = true;
        digit = firstChar;
      }
      else if sStartsTxtNum
      then {
        present = true;
        digit = numMapG.${startingTxtNum};
      }
      else {present = false;};
  in
    ans;

  firstNumberUsingMap = numMapG: s: let
    p = prefixNumber numMapG s;
    strLen = builtins.stringLength s;
    restString = builtins.substring 1 strLen s;
    recur = firstNumberUsingMap numMapG restString;
    calc =
      if p.present
      then p.digit
      else recur;
    ans =
      if strLen == 0
      then null
      else calc;
  in
    ans;

  firstNumber' = firstNumberUsingMap numMap;
  lastNumber' = s: let
    revMatch = firstNumberUsingMap numMapRev;
    reversed = revString s;
    ans = revMatch reversed;
  in
    ans;

  calibrationValue' = calibrationValueGeneric firstNumber' lastNumber';
  calibrationSum' = calibrationSumGeneric calibrationValue';

  part2 = calibrationSumForInput calibrationSum';
}
